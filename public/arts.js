$(document).ready(function () {
    $(".arts").slick({
        dots: true,
        infinite: true,
        arrow: true,
        autoplay: true,
        autoplaySpeed: 10000,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 720,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }]
    });
});

